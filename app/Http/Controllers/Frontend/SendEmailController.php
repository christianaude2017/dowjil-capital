<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Mail\SendMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class SendEmailController extends Controller
{
    //
    public function send(Request $request){


        $this->validate($request, [
            'nom'     =>  'required',
            'email'  =>  'required|email',
            'sujet' => 'required',
            'message' =>  'required'
        ]);

        $data = array(
            'nom'      =>  $request->nom,
            'message'   =>   $request->message,
            'sujet' => $request->sujet,
            'email' => $request->email
        );


        Mail::to('infos@dowjilcapital.com')->send(new SendMail($data));
        return back()->with('success', 'Thanks for contacting us!');

    }
}
